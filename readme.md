## Community detection with unsupervised learning tools
This project is about detecting and splitting a graph of relations between individuals in an automated fashion.

## Technologies and frameworks used
All the scripts in the ``src/`` folder are written in <i>Python 3.7.3 (release: Oct. 7 2019)</i>.
The libraries used for this project are as follows:
- [NetworkX 2.4](https://networkx.github.io/documentation/stable/) for network tools
- [Plot.ly 4.4.1](https://plot.ly/python/) for data rendering
- [Numpy 1.18.0](https://numpy.org/doc/)
- [SciPy 1.4.1](https://docs.scipy.org/doc/scipy/reference/)
- [ColorLover](https://github.com/plotly/colorlover)

## How to use ?
To run the software you will have to put all your input data in the same directory. Let's say ``sample/my_input/``. 
This folder should at least contain a file ``my_input.dat`` providing the list of connections forming the network.
Note that this file must mention at its head the number of nodes contained in the graph.
If you want, you can also put in this same folder a ``community.dat`` file, storing the expected partitioning from the dataset. 
There are already some dataset at your disposal in the ``sample/`` directory.
To run the scripts, at the root folder of this project type the following in your favorite command line:

```
python3.7 src/community_detection.py <input_file> <output_folder> <dim_ratio> [-e | --explicit]
```
Where:
- ``<input_file>`` is the path to your data file
- ``<output_folder>`` is the folder to put outputs in. Make sure its name is followed by ``/`` (see example bellow).
- ``<dim_ratio>`` is the ratio of dimensionalities to take into account during the computing. Beware that with a set of 200+ nodes this will start to take a while for computing the best splitting configurationfrom a given dimension. The best would be to take into account the 10 or 15 first dimension for distance calculation.
- ``-e`` or ``--explicit`` are options to display the work in progress results

For instance with the preconfigured dataset you could type the following from the root folder's project:
```
python3.7 src/community_detection.py sample/zachary/zachary.dat out/ 0.5 -e
```
In this dataset of 32 nodes, the network will be splitted optimal by taking into account 1 to 16 dimensions in its spectral layout.
The output will be saved in ``out/zachary_optimal_stacked_views.html``. You will then be able to view and interact with the clustering results on a web browser.
