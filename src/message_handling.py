#!/usr/bin/python3.7
# coding: utf-8
def show_graph(graph, explicit_mode):
    if explicit_mode == True:
        print("➜  Graph:")
        print("   ⚙  Type: Undirected")
        print("   ⚙  Number of nodes: {}".format(graph.number_of_nodes()))
        print("   ⚙  Number of edges: {}".format(graph.number_of_edges()))
        print("   ⚙  Nodes:")
        for node in graph.nodes():
            print("      ⇢  Node#{:03}  ⌅ cluster: {:02}".format(
                node,
                graph.nodes[node]['cluster'] if 'cluster' in graph.nodes[node] else int('nan')
            ))
        print("   ⚙  Edges:")
        for source, destination in graph.edges():
            print("      ⇢  Node#{:03} ⇌  Node#{:03}".format(
                source,
                destination
            ))

def show_message(message, explicit_mode):
    if explicit_mode == True:
        print(message)