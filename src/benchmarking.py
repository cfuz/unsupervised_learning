import sys
import os


sample_dir = "sample/"
bin_file = "src/community_detection.py"
input_files = [
    sample_dir + "network_10/network_10.dat",
    sample_dir + "zachary/zachary.dat",
    sample_dir + "network_50/network_50.dat",
    sample_dir + "network_100/network_100.dat",
    sample_dir + "network_250/network_250.dat",
    sample_dir + "network_500/network_500.dat",
    sample_dir + "network_750/network_750.dat",
    sample_dir + "network_1000/network_1000.dat",
]
output_dir = "out/"
n_nodes = [ 
    10, 
    34, 
    50, 
    100, 
    250, 
    500, 
    750, 
    1000
]
cpu_specs = "8@4.9GHz"


for set_index, input_file in enumerate(input_files):
    print("➜  {}".format(input_file))
    for benchmark_index in range(5):
        print("   ϟ  benchmark#{}".format(benchmark_index))
        command_line = "python3.7 {} {} {} {:.5f}".format(
            bin_file,
            input_file,
            output_dir,
            10 / n_nodes[set_index]
        )
        os.system(command_line)
