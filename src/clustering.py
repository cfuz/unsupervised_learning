#!/usr/bin/python3.7
# coding: utf-8
import networkx as nx
import numpy as np
import scipy.spatial.distance as ssd
import math

from message_handling import (
    show_message
)
from metrics import (
    cosine_matrix, 
    euclidean_matrix
)
from scipy.cluster.hierarchy import (
    single, 
    complete, 
    fcluster
)


MAX_UNCHANGED_ITERATION = 5


def modularity_from_graph(graph):
    modularity = 0.0
    n_edges = graph.number_of_edges()
    adjacency_matrix = nx.adjacency_matrix(graph).toarray()
    for node_1_index, node_1_data in graph.nodes.items():
    	# On parcourt la liste des noeuds adjacents au noeud courant
    	for node_2_index, node_2_data in graph.nodes.items():
    	    # Si le noeud adjacent est contenu dans le meme cluster que
    	    # le present noeud, on ajoute +1 a la modularite, -1 sinon
    	    if node_2_data['cluster'] == node_1_data['cluster']:
                modularity += adjacency_matrix[node_1_index - 1][node_2_index - 1] 
                modularity -= (graph.degree[node_1_index] * graph.degree[node_2_index]) / (2 * n_edges)
    return modularity / (2 * n_edges)

def modularity(graph, partition, adjacency_matrix, n_edges):
    modularity = 0.0
    for node_1_index, node_1_cluster in enumerate(partition):
    	# On parcourt la liste des noeuds adjacents au noeud courant
    	for node_2_index, node_2_cluster in enumerate(partition):
    	    # Si le noeud adjacent est contenu dans le meme cluster que
    	    # le present noeud, on ajoute +1 a la modularite, -1 sinon
    	    if node_2_cluster == node_1_cluster:
                modularity += adjacency_matrix[node_1_index][node_2_index] 
                modularity -= (graph.degree[node_1_index + 1] * graph.degree[node_2_index + 1]) / (2 * n_edges)
    return modularity / (2 * n_edges)

# Labellise les noeuds en fonction de la partition fournie en parametre
def update_labels(graph, partition):
    for index, cluster_id in enumerate(partition):
        graph.nodes[index + 1]['cluster'] = cluster_id
    return graph

def gen_condensed_distance_matrix(points, distance_type):
    distance_matrix = []
    if distance_type == "ang":
        distance_matrix = cosine_matrix(points)
    elif distance_type == "euc":
        distance_matrix = euclidean_matrix(points)
    else:
        print("✘  clustering::gen_cluster() 3rd argument is invalid. Expected 'ang' or 'euc' gave {}".format(distance))
        return None
    # Conversion de la matrice redondante ainsi obtenue en matrice condensee
    # (i.e. on ne conserve que la matrice trinagulaire superieure de cette 
    # matrice en stockant les valeurs ainsi observees dans un vecteur 1D)
    return ssd.squareform(distance_matrix)

def gen_linkage_matrix(condensed_distance_matrix, linkage_type):
    linkage_matrix = []
    # .. single linkage
    # On commence par creer l'arborescence des parentees entre les differents noeuds
    # a partir de la matrice des distances fournies en parametre de la fonction. Le
    # resultat obtenu est un dendrogramme stocke sous forme de liste de tables constituees de
    # quatres elements. Une table correspond a une iteration dans le processus de clustering:
    # -- table[1] & table[2] definissent les deux clusters qui ont ete fusionnes
    # -- table[3] definit la distance entre ces deux clusters
    # -- table[4] declare le nombre d'elements du nouveau cluster ainsi forme
    #   --> Source:
    #   https://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.single.html
    if linkage_type == "sing":
        linkage_matrix = single(condensed_distance_matrix)
    # .. complete linkage
    elif linkage_type == "comp":
        linkage_matrix = complete(condensed_distance_matrix)
    else:
        print("✘  clustering::gen_cluster() 4th argument is invalid. Expected 'sing' or 'comp' gave {}".format(linkage))
        return None
    return linkage_matrix

def find_optimal_clustering(graph, linkage_matrix, distance_type, linkage_type, explicit, filename, dim): 
    # Le fichier servira de trace pour la recuperation des donnees concernant 
    # la modularite
    file = open(filename, "a")
    maximized_modularity, maximized_partitioning = -math.inf, []
    # On extrait la matrice d'adjacence du graphe
    adjacency_matrix = nx.adjacency_matrix(graph).toarray()
    # On recupere le nombre de connexions entre les noeuds pour le calcul de modularite
    n_edges = nx.number_of_edges(graph)
    counter = 0
    for index, entry in enumerate(reversed(linkage_matrix[:int(len(linkage_matrix) * .985)])):
        # On recupere la distance determinant la fusion des noeuds dans un meme cluster
        height = entry[2]
        partition = fcluster(
            linkage_matrix, 
            t = height, 
            criterion = 'distance'
        )
        # On calcule enfin la modularite des partitions ainsi obtenues
        mod = modularity(graph, partition, adjacency_matrix, n_edges)
        file.write("{} {}\t{:03}\t{:04}\t{:01.5f}\n".format(
            distance_type,
            linkage_type,
            dim,
            index,
            mod
        ))
        if maximized_modularity < mod:
            show_message(
                "   ϟ  it#{:03} modularity: {:01.5f}\t[dim: {:03}, dist: {}, link: {}]".format(   
                    index, 
                    mod,
                    dim,
                    distance_type,
                    linkage_type,
                ),
                explicit
            )
            maximized_modularity = mod
            maximized_partitioning = partition
            counter = 0
        else:
            counter += 1
            if counter == MAX_UNCHANGED_ITERATION:  
                break
    file.close()
    show_message(
        "➜  opt. modularity: {:01.5f}\t\t[dim: {:03}, dist: {}, link: {}]".format(
            maximized_modularity,
            dim,
            distance_type,
            linkage_type,
        ),
        explicit
    )
    return { 
        'mod': maximized_modularity, 
        'partition': maximized_partitioning, 
        'dim': dim
    }

def gen_spectral_layout(graph, dim):
    # On recupere les positions des noeuds dans le spectre du graphe ainsi 
    # determine.. via la bibliotheque networkx, qui recupere les coordonnees 
    # du spectre du laplacien non normalise
    spectral_layout = nx.spectral_layout(graph, dim = dim)
    # On en deduit la liste des coordonnees a partir desquelles determiner 
    #la matrice des distances correspondante
    #   --> Nota: 
    #   La fonction de networkx permettant de retourner les positions
    #   spectrales des points du graphe semble etre plus fidele au niveau 
    #   de la representation graphique, que lorsque l'on passe par nos propres 
    #   vecteurs propres.
    points = []
    for key in spectral_layout:
        points.append(np.array(spectral_layout[key]))
    return points



def gen_clusters(graph, points, dim, distance_type, linkage_type, explicit, filename):
    show_message(
        "➜  starting clustering..\t\t[dim: {:03}, dist: {}, link: {}]".format(
            dim,
            distance_type,
            linkage_type,
        ),
        explicit
    )
    # D'ou la matrice des distances en fonction du type de distance voulue
    condensed_distance_matrix = gen_condensed_distance_matrix(points, distance_type)
    # On en deduit la partition du graphe
    linkage_matrix = gen_linkage_matrix(condensed_distance_matrix, linkage_type)
    # On exploite ensuite ces donnees pour determiner a une hauteur de dendrogramme donnee
    # l'appartenance de chaque noeud a un cluster via la fonction fcluster() de scipy.
    #   --> Nota:
    #   La sortie de fcluster est une table de taille egale au nombre de noeuds contenus dans le 
    #   graphe. 
    #       ∀ i∈ V, fcluster(linkage_matrix, ..)[i] = cluster_id
    #   --> Source:
    #   https://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.fcluster.html#scipy.cluster.hierarchy.fcluster
    return find_optimal_clustering(graph, linkage_matrix, distance_type, linkage_type, explicit, filename, dim)

