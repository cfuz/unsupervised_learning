#!/usr/bin/python3.7
# coding: utf-8
import plotly.graph_objects as go
import colorlover as cl
import numpy as np

from plotly.subplots import make_subplots



def gen_3d_edge_trace(dataset):
    x_edges, y_edges, z_edges = [], [], []
    for edge in dataset:
        x_edges += [ edge['src'][0], edge['dest'][0], None ]
        y_edges += [ edge['src'][1], edge['dest'][1], None ]
        z_edges += [ edge['src'][2], edge['dest'][2], None ]
    return (x_edges, y_edges, z_edges)

def gen_3d_node_trace(dataset):
    x_nodes, y_nodes, z_nodes, labels, clusters = [], [], [], [], []
    for cluster in dataset:
        for node in dataset[cluster]:
            x_nodes.append(node['value'][0])
            y_nodes.append(node['value'][1])
            z_nodes.append(node['value'][2])
            labels.append(node['label'] + '<extra>Cluster#' + str(cluster) + '</extra>') 
            clusters.append(cluster)
    return (x_nodes, y_nodes, z_nodes, labels, clusters)

def gen_3d_scattered_edges(dataset, key):
    x_edges, y_edges, z_edges = gen_3d_edge_trace(dataset)
    scattered_edges = go.Scatter3d(
        x = x_edges, 
        y = y_edges,
        z = z_edges,
        mode = 'lines',
        opacity = 0.5,
        line = dict(
            color = 'rgb(88,110,117)', 
            width = 1,
        ),
        hoverinfo = 'none',
        name = "{} 3D edges".format(key)
    )
    return scattered_edges

def gen_3d_scattered_nodes(dataset, key):
    x_nodes, y_nodes, z_nodes, labels, clusters = gen_3d_node_trace(dataset)
    scattered_nodes = go.Scatter3d(
        x = x_nodes,           
        y = y_nodes,
        z = z_nodes,
        mode = 'markers',
        marker = dict(
            showscale = False,
            symbol = 'circle',
            size = 3,
            color = clusters,
            colorscale = 'Viridis',
            opacity = 0.75,
            colorbar = dict(
                thickness = 5,
                title = 'Cluster index',
                xanchor = 'left',
                titleside = 'right'
            ),
            #line = dict(
            #    color = 'rgb(88,110,117)', 
            #    width = 0.5,
            #),
        ),
        hovertemplate = "%{text}",
        text = labels,
        name = "{} 3D nodes".format(key),
        showlegend = False,
    )
    return scattered_nodes


def gen_2d_edge_trace(dataset):
    x_edges, y_edges = [], []
    for edge in dataset:
        x_edges += [ edge['src'][0], edge['dest'][0], None ]
        y_edges += [ edge['src'][1], edge['dest'][1], None ]
    return (x_edges, y_edges)

def gen_2d_node_trace(dataset):
    x_nodes, y_nodes, z_nodes, labels, clusters = [], [], [], [], []
    for cluster in dataset:
        for node in dataset[cluster]:
            x_nodes.append(node['value'][0])
            y_nodes.append(node['value'][1])
            labels.append(node['label'] + '<extra>Cluster#' + str(cluster) + '</extra>')
            clusters.append(cluster)
    return (x_nodes, y_nodes, labels, clusters)

def gen_2d_scattered_nodes(dataset, key):
    x_nodes, y_nodes, labels, clusters = gen_2d_node_trace(dataset)
    scattered_nodes = go.Scatter(
        x = x_nodes,
        y = y_nodes,
        mode = 'markers',
        marker = dict(
            showscale = False,
            symbol = 'circle',
            size = 8,
            color = clusters,
            colorscale = 'Viridis',
            colorbar = dict(
                thickness = 5,
                title = 'Cluster index',
                xanchor = 'left',
                titleside = 'right'
            ),
            line = dict(
                color = 'rgb(88,110,117)', 
                width = 0.5,
            ),
        ),
        hovertemplate = "%{text}",
        text = labels,
        name = "{} 2D nodes".format(key),
        showlegend = False,
    )
    return scattered_nodes

def gen_2d_scattered_edges(dataset, key):
    x_edges, y_edges = gen_2d_edge_trace(dataset)
    scattered_edges = go.Scatter(
        x = x_edges, 
        y = y_edges,
        mode = 'lines',
        opacity = 0.5,
        line = dict(
            color = 'rgb(88,110,117)', 
            width = 0.75,
        ),
        hoverinfo = 'none',
        name = "{} 2D edges".format(key)
    )
    return scattered_edges

def gen_scattered_modularity(modularity_trace):
    scatter_plots = []
    for index, variant in enumerate(modularity_trace):
        x, y = [], []
        for point in modularity_trace[variant]:
            x += [point[0]]
            y += [point[1]]
        scatter_plots += [ go.Scatter(
            x = x, 
            y = y, 
            mode = "lines+markers",
            name = "{} mod. evo.".format(variant)
        ) ]
    return scatter_plots

def gen_scattered_benchmarks_fixed_dim(avg_time_fixed_dim):
    scatter_plots = []
    for n_nodes in avg_time_fixed_dim:
        x, y = [], []
        for dim in avg_time_fixed_dim[n_nodes]:
            x += [ dim ]
            y += [ avg_time_fixed_dim[n_nodes][dim] ]
        scatter_plots += [ go.Scatter(
            x = x,
            y = y,
            mode = "lines+markers",
            name = "{} nodes avg time res. (fixed dim.)".format(n_nodes)
        ) ]
    return scatter_plots

def gen_scattered_benchmarks_fixed_nodes(avg_time_fixed_nodes):
    scatter_plots = []
    x, y = [], []
    for n_nodes in avg_time_fixed_nodes:
        x += [ n_nodes ]
        y += [ avg_time_fixed_nodes[n_nodes] ]
    scatter_plots += [ go.Scatter(
        x = x,
        y = y,
        mode = "lines+markers",
        name = "Avg time res. by graph size"
    ) ]
    return scatter_plots

def draw_stacked_benchmark_view(
    scatter_plots, 
    title, 
    subplot_titles, 
    specs, 
    output_filename
):
    fig = make_subplots(
        rows = 1, 
        cols = 2,
        specs = specs,
        subplot_titles = subplot_titles
    )
    for scatter in scatter_plots[:len(scatter_plots)-1]:
        fig.add_trace(scatter, row = 1, col = 1)
    fig.add_trace(scatter_plots[len(scatter_plots) - 1], row = 1, col = 2)
    fig.update_layout(
        showlegend = True, 
        title_text = title,
        template = "plotly_white",
        hovermode = 'closest',
        yaxis_type = "log"
    )
    fig.update_xaxes(title_text = "Dim", row = 1, col = 1)
    fig.update_yaxes(title_text = "Avg. res. time (s)", row = 1, col = 1)
    fig.update_xaxes(title_text = "Number of nodes", row = 1, col = 2)
    fig.update_yaxes(title_text = "Avg. res. time (s)", row = 1, col = 2)
    fig.write_html(output_filename)



'''
Pour l'affichage groupe des meilleurs partitionements de graphe en fonction du 
mode de calcul pour une largeur de spectre donnee. La dernier cellule de 
l'affichage devant rendre compte de l'evolution des optimaux de modularite en
fonction de la taille du spectre (i.e. le nombre de vecteurs propres que l'on
considere pour le calcul des distances).
'''
def draw_stacked_network_view(
    scatter_plots, 
    title, 
    subplot_titles, 
    specs, 
    expected, 
    output_filename
):
    n_rows = len(specs)
    n_cols = 4
    fig = make_subplots(
        rows = n_rows, 
        cols = n_cols,
        specs = specs,
        subplot_titles = subplot_titles
    )
    row, col = 1, 1
    # On itere sur les premiers elements de la liste contenant les vues de partitionement
    # par mode, on met a jour la figure contenant les graphes a afficher puis l'on
    # supprime l'element de la liste.
    for scatter in scatter_plots[:]:
        fig.add_trace(scatter[1], row = row, col = col)
        fig.add_trace(scatter[0], row = row, col = col)
        scatter_plots.remove(scatter)
        col += 1
        if col > n_cols:
            col = 1
            row += 1
        if row == n_rows:
            break
    if expected:
        fig.add_trace(scatter_plots[0][1], row = n_rows, col = 3)
        fig.add_trace(scatter_plots[0][0], row = n_rows, col = 3)
        del scatter_plots[0]
        fig.add_trace(scatter_plots[0][1], row = n_rows, col = 4)
        fig.add_trace(scatter_plots[0][0], row = n_rows, col = 4)
        del scatter_plots[0]
    # Arrive a ce stade il nous faut maintenant empiler sur une meme figure les 
    # differentes evolution de l'optimal de modularite en fonction du mode et 
    # de la dimension d'analyse
    for scatter in scatter_plots:
        fig.add_trace(scatter, row = n_rows, col = 1)
    fig.update_layout(
        showlegend = True, 
        title_text = title,
        template = "plotly_white",
        margin = dict(t = 100),
        hovermode = 'closest'
    )
    fig.update_xaxes(title_text = "Dim", row = n_rows, col = 1)
    fig.update_yaxes(title_text = "Opt. modularity", row = n_rows, col = 1)
    fig.write_html(output_filename)


def gen_2d_layout(title):
    axis = dict(
        showbackground = True,
        showline = False,
        zeroline = True,
        showgrid = False,
        showticklabels = True,
        title = ''
    )
    return go.Layout(
        title = {
            'text': title,
            'y': 0.95,
            'x': 0.5,
            'xanchor': 'center',
            'yanchor': 'top',
        },
        width = 1000,
        height = 1000,
        showlegend = True,
        legend = {
            'xanchor': 'right',
        },
        template = "plotly_white",
        scene = dict(
            xaxis = dict(axis),
            yaxis = dict(axis)
        ),
        margin = dict(
            t = 100
        ),
        hovermode = 'closest',
    )

'''
Genere un graphe au format html via une instance de graphe, ainsi que la liste des coordonnees de 
chaque noeuds le composant. La sortie est enregistree au format .html dans le fichier 
output_filename.
'''
def draw_2d_network(dataset, title, output_filename):
    x_edges, y_edges = gen_2d_edge_trace(dataset)
    x_nodes, y_nodes, labels, clusters = gen_2d_node_trace(dataset)
    scattered_edges = go.Scatter(
        x = x_edges,
        y = y_edges,
        mode = 'lines',
        line = dict(
            color = 'rgb(88,110,117)', 
            width = 1
        ),
        hoverinfo = 'none'
    )
    scattered_nodes = go.Scatter(
        x = x_nodes,
        y = y_nodes,
        mode = 'markers',
        marker = dict(
            showscale = True,
            symbol = 'circle',
            size = 10,
            color = clusters,
            colorscale = 'Magma',
            colorbar = dict(
                thickness = 5,
                title = 'Cluster index',
                xanchor = 'left',
                titleside = 'right'
            ),
            line = dict(
                color = 'rgb(88,110,117)',
                width = 0.5
            )
        ),
        hovertemplate = "%{text}",
        text = labels,
    )
    layout = gen_2d_layout(title)
    fig = go.Figure(
        data = [ scattered_edges, scattered_nodes],
        layout = layout
    )
    fig.write_html(output_filename)



'''
Retourne la configuration / mise en page du graphique a generer
'''
def gen_3d_layout(title):
    axis = dict(
        showbackground = True,
        showline = True,
        zeroline = True,
        showgrid = False,
        showticklabels = True,
        title = ''
    )
    return go.Layout(
        title = {
            'text': title,
            'x': 0.5,
            'y': 0.95,
            'xanchor': 'center',
            'yanchor': 'top',
        },
        width = 1000,
        height = 1000,
        showlegend = True,
        legend = {
            'xanchor': 'right',
        },
        template = "plotly_white",
        scene = dict(
            xaxis = dict(axis),
            yaxis = dict(axis),
            zaxis = dict(axis)
        ),
        margin = dict(
            t = 100
        ),
        hovermode = 'closest',
    )

'''
Genere un graphe en 3D du jeu de donnees fournis en parametres.
    --> Source:
    https://plot.ly/python/v3/3d-network-graph/
'''
def draw_3d_network(dataset, title, output_filename):
    x_edges, y_edges, z_edges = gen_3d_edge_trace(dataset)
    x_nodes, y_nodes, z_nodes, labels, clusters = gen_3d_node_trace(dataset)
    scattered_edges = go.Scatter3d(
        x = x_edges, 
        y = y_edges,
        z = z_edges,
        mode = 'lines',
        line = dict(
            color = 'rgb(88,110,117)', 
            width = 0.5,
            opacity = 0.5
        ),
        hoverinfo = 'none',
        name="edges"
    )
    scattered_nodes = go.Scatter3d(
        x = x_nodes,           
        y = y_nodes,
        z = z_nodes,
        mode = 'markers',
        marker = dict(
            showscale = True,
            symbol = 'circle',
            size = 4,
            color = clusters,
            colorscale = 'Magma',
            colorbar = dict(
                thickness = 5,
                title = 'Cluster index',
                xanchor = 'left',
                titleside = 'right'
            ),
            line = dict(
                color = 'rgb(88,110,117)', 
                width = 0.5,
                opacity = 0.25
            )
        ),
        hovertemplate = "%{text}",
        text = labels,
        name = "nodes"
    )
    layout = gen_3d_layout(title)
    fig = go.Figure(
        data = [ scattered_edges, scattered_nodes],
        layout = layout
    )
    fig.write_html(output_filename)

