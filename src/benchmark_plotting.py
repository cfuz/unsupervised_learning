import sys
import numpy
import plotly

from drawing import (
    gen_scattered_benchmarks_fixed_dim,
    gen_scattered_benchmarks_fixed_nodes,
    draw_stacked_benchmark_view
)


input_file = sys.argv[1]
output_dir = sys.argv[2]
dataset = {}

file = open(input_file, 'r')
for entry in file:
    line = entry.split("\n")[0].split("\t")
    n_nodes, dim, time = int(line[0]), int(line[1]), float(line[2])
    if n_nodes not in dataset:
        dataset[n_nodes] = { dim: [ time ] }
    else:
        if dim not in dataset[n_nodes]:
            dataset[n_nodes][dim] = [ time ]
        else:
            dataset[n_nodes][dim].append(time)
file.close()

# Calcule la moyenne de temps de résolution par dimension pour un nombre 
# de noeuds donné
avg_time_fixed_dim = {}
for n_nodes in dataset:
    if n_nodes not in avg_time_fixed_dim:
        avg_time_fixed_dim[n_nodes] = {}
    for dim in dataset[n_nodes]:
        avg = 0.0
        for time in dataset[n_nodes][dim]:
            avg += time
        avg /= len(dataset[n_nodes][dim])
        avg_time_fixed_dim[n_nodes][dim] = avg

# On calcule le temps moyen mis pour la résolution sur une dimension 
avg_time_fixed_nodes = {}
for n_nodes in avg_time_fixed_dim:
    avg, counter = 0.0, 0
    for dim in avg_time_fixed_dim[n_nodes]:
        avg += avg_time_fixed_dim[n_nodes][dim]
        counter += 1
    avg_time_fixed_nodes[n_nodes] = avg / counter
    
# On trace les courbes superposées liées à l'évaluation des performances
scatter_plots = gen_scattered_benchmarks_fixed_dim(avg_time_fixed_dim)
scatter_plots += gen_scattered_benchmarks_fixed_nodes(avg_time_fixed_nodes)
subplot_titles = [
    "<i>Average time resolution with increasing dimensionality by graph's size</i>",
    "<i>Average time resolution with increasing graph's size (accross all dimensions)</i>"
]
specs = [[ {"type": "scatter"}, {"type": "scatter"} ]]
draw_stacked_benchmark_view(
    scatter_plots, 
    "<b>Performance evaluation given the number of nodes and non-trivial eigen vectors taken into account</b>",
    subplot_titles,
    specs,
    output_dir + "benchmark.html"
)