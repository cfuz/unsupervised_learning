#!/usr/bin/python3.7
# coding: utf-8
import sys
import os
import networkx as nx
import math
import threading
import time

from message_handling import (
    show_message,
    show_graph
)
from parsing import (
    parse_arguments,
	parse_graph, 
	parse_clusters
)
from drawing import (
    gen_scattered_modularity, 
    gen_3d_scattered_nodes,
    gen_3d_scattered_edges,
    gen_2d_scattered_nodes,
    gen_2d_scattered_edges,
    draw_stacked_network_view
)
from clustering import (
	gen_clusters, 
	gen_spectral_layout, 
	modularity,
	modularity_from_graph
)


METAPARAMETERS = {}
LOCKER = threading.Lock()
MODULARITY_DATASET = { 'optimal': {}, 'trace': {}, 'points': {} }


def update_modularity_dataset(mod, distance_type, linkage_type):
    key = "{} {}".format(distance_type, linkage_type)
    # Si la modularite concernant le mode 'key' est plus grande que celle deja
    # presente, on met a jour le dictionnaire a l'entree correspondante
    if key in MODULARITY_DATASET['optimal']:
        if MODULARITY_DATASET['optimal'][key]['mod'] < mod['mod']:
            MODULARITY_DATASET['optimal'][key] = mod
    else:
        MODULARITY_DATASET['optimal'][key] = mod
    # On met a jour la table d'evolution de la modularite en fonction de la dimesion
    if key in MODULARITY_DATASET['trace']:
        MODULARITY_DATASET['trace'][key].append((mod['dim'], mod['mod']))
    else:
        MODULARITY_DATASET['trace'][key] = [(mod['dim'], mod['mod'])]

def threaded_benchmark(graph, dim, points, distance_mode, linkage_mode, filename):
    modularity = gen_clusters(
        graph,
        points,
        dim,
        distance_mode,
        linkage_mode,
        METAPARAMETERS['explicit'],
        filename
    )
    with LOCKER:
        update_modularity_dataset(
            modularity,
            distance_mode,
            linkage_mode
        )

def benchmark_modularity(graph, mod_dict, points, dim, filename):
    distance_modes, linkage_modes = ["euc", "ang"], ["sing", "comp"]
    threads = []
    for distance_mode in distance_modes:
        for linkage_mode in linkage_modes:
            threads.append(threading.Thread(
                target = threaded_benchmark, 
                args = (graph, dim, points, distance_mode, linkage_mode, filename)
            ))
    t_start = time.process_time()
    for thread in threads:
        thread.start()
    for thread in threads:
        thread.join()
    t_stop = time.process_time()
    # On inscrit le temps de résolution dans un fichier prévu à cet effet
    benchmark_file = open(METAPARAMETERS['benchmark_data'], "a")
    benchmark_file.write("{}\t{:03}\t{:.5f}\n".format(
        graph.number_of_nodes(),
        dim,
        t_stop - t_start
    ))
    benchmark_file.close()
    # On annote les noeuds du graphe si le jeu de données dispose d'une réalité 
    # de terrain
    if METAPARAMETERS['expected']:
        mod_dict['expected'] = modularity_from_graph(graph)
    return mod_dict    

def gen_network_computed_clustering_dataset(graph, points, partition):
    # On constitue le dataset pour visualisation du graphe
    cluster_dataset = {}
    for node_id in graph.nodes():
        dataset_item = { 
            'value': (
                points[node_id - 1][0], 
                0.0 if len(points[node_id - 1]) <= 1 else points[node_id - 1][1],
                0.0 if len(points[node_id - 1]) <= 2 else points[node_id - 1][2],
            ), 
            'label': 'Node#' + str(node_id),
        }
        if partition[node_id - 1] in cluster_dataset:            
            cluster_dataset[partition[node_id - 1]].append(dataset_item)
        else:
            cluster_dataset[partition[node_id - 1]] = [ dataset_item ]
    return cluster_dataset
    
# On recupere les arcs du graphe pour insertion dans le jeu de representation
def gen_network_edges_dataset(graph, points):
    edges = []
    for edge in graph.edges():
        edges.append({
            'src': points[edge[0] - 1],
            'dest': points[edge[1] - 1]
        })
    return edges

def gen_network_expected_clustering_dataset(graph):
    # Enfin si une base de partition existe nous l'inserons dans le dataset
    cluster_dataset = {}
    for node_id, node_data in graph.nodes.items():
        dataset_item = {
            'value': (
                MODULARITY_DATASET['points'][node_id - 1][0],
                0.0 if len(MODULARITY_DATASET['points'][node_id - 1]) <= 1 else MODULARITY_DATASET['points'][node_id - 1][1],
                0.0 if len(MODULARITY_DATASET['points'][node_id - 1]) <= 2 else MODULARITY_DATASET['points'][node_id - 1][2],
            ),
            'label': 'Node#' + str(node_id),
        }
        if node_data['cluster'] in cluster_dataset:
            cluster_dataset[node_data['cluster']].append(dataset_item)
        else:
            cluster_dataset[node_data['cluster']] = [dataset_item]
    return cluster_dataset

def render_stacked(network_dataset, modularity_dataset):
    show_message("➜  Rendering stacked views..", METAPARAMETERS['explicit'])
    # Pour chaque mode, on genere la figure correspondante que l'on va stocker 
    # temporairement dans une table
    scatter_plots, subplot_titles, row_specs, specs = [], [], [], []
    counter, n_couple_figures = 0, 2
    for key in network_dataset['clusters']:
        scatter_plots += [(
            gen_3d_scattered_nodes(network_dataset['clusters'][key], key), 
            gen_3d_scattered_edges(network_dataset['edges'], key)
        ), (
            gen_2d_scattered_nodes(network_dataset['clusters'][key], key),
            gen_2d_scattered_edges(network_dataset['edges'], key)
        )]
        subplot_titles += [
            "<i>{}@{:01.3f}: 3D optimal net. clustering [{}]<br>#clusters: {}</i>".format(
                METAPARAMETERS['input_basename'] ,
                MODULARITY_DATASET['optimal'][key]['mod'],
                key,
                len(network_dataset['clusters'][key])
            ),
            "<i>2D perspective</i>"
        ]
        row_specs += [{"type": "scene"}, {"type": "scatter"}]
        counter += 1
        if counter == n_couple_figures:
            specs += [row_specs]
            row_specs = []
            counter = 0
    subspecs = [ {"type": "scatter", "colspan": 2}, None ]
    subplot_titles.append(
        "<i>Opt. mod. evolution with increasing dimensionality ({})</i>".format(
            METAPARAMETERS['input_basename'] ,
        )
    )
    # Si un template de partition existe dans le dossier des donnees d'entrees,
    # On l'ajoute a la page de rendu
    if MODULARITY_DATASET['expected']:
        scatter_plots += [(
            gen_3d_scattered_nodes(network_dataset['expected'], 'expected'), 
            gen_3d_scattered_edges(network_dataset['edges'], 'expected')
        ), (
            gen_2d_scattered_nodes(network_dataset['expected'], 'expected'),
            gen_2d_scattered_edges(network_dataset['edges'], 'expected')
        )]
        subplot_titles += [
            "<i>{}@{:01.3f}: 3D expected splitting<br>#clusters: {}</i>".format(
                METAPARAMETERS['input_basename'] ,
                MODULARITY_DATASET['expected'],
                len(network_dataset['expected'])
            ),
            "<i>2D perspective</i>"
        ]
        subspecs += [ {"type": "scene"}, {"type": "scatter"} ]
    else:
        subspecs += [ None, None ]
    specs += [subspecs]
    # On ajoute enfin le graphe montrant l'evolution de la modularite en  
    # fonction du mode de calcul
    scatter_plots += gen_scattered_modularity(MODULARITY_DATASET['trace'])
    draw_stacked_network_view(
        scatter_plots, 
        "<b>Optimal network clustering representations given the mode and modularity evolution</b>",
        subplot_titles,
        specs,
        METAPARAMETERS['expected'],
        "{}_opt_setup.html".format(METAPARAMETERS['output_html'])
    )


if __name__ == "__main__":
    # On configure les métaparamètres du programme à partir des données 
    # renseignées dans la ligne de commande à l'appel du programme
    METAPARAMETERS = parse_arguments()
    # On construit le graphe a partir du fichier fournit en argument a l'appel du script
    graph = parse_graph(METAPARAMETERS['input'])
    METAPARAMETERS['expected'] = parse_clusters(
        graph, 
        METAPARAMETERS['communities']
    )
    if METAPARAMETERS['expected']:
        show_message(
            "✔  Cluster data base has been detected. Graph has been updated with expected split!", 
            METAPARAMETERS['explicit']
        )
    else:
        show_message(
            "✘  No cluster data base. I will not be able to display expected splitting results..",
            METAPARAMETERS['explicit']
        )
    show_graph(graph, METAPARAMETERS['explicit'])
    # A partir de ce graphe on va chercher a trouver le jeu de donner optimal 
    # maximisant la modularite d'une partition pour un type de distance et 
    # une methode de partitionement donnee
    for dimensionality in range(
        2, 
        int(graph.number_of_nodes() * float(sys.argv[3]))
    ):
        points = gen_spectral_layout(graph, dimensionality)
        if dimensionality in range(1, 4):
            MODULARITY_DATASET['points'] = points 
        MODULARITY_DATASET = benchmark_modularity(
            graph, 
            MODULARITY_DATASET,
            points,
            dimensionality, 
            METAPARAMETERS['output_data']
        )
    # Nous pouvons desormais creer les jeux de donnees pour la representation
    # graphique des partitions ainsi trouvees
    network_dataset = { 'clusters': {} }
    for key in MODULARITY_DATASET['optimal']:
        network_dataset['clusters'][key] = gen_network_computed_clustering_dataset(
            graph, 
            MODULARITY_DATASET['points'],
            MODULARITY_DATASET['optimal'][key]['partition'],
        )
    network_dataset['edges'] = gen_network_edges_dataset(
        graph,
        MODULARITY_DATASET['points']
    )
    network_dataset['expected'] = gen_network_expected_clustering_dataset(graph)
    show_message("✔  Done!", METAPARAMETERS['explicit'])
    render_stacked(network_dataset, MODULARITY_DATASET)
