#!/usr/bin/python3.7
# coding: utf-8
import math
import numpy as np
from scipy.spatial import distance


EPSILON = 1.0e-8


'''
Sources des metriques: 
https://cmry.github.io/notes/euclidean-v-cosine
'''


'''
Le cosine examine l'angle entre les vecteurs x et y fournis en parametres.
Ce type de mesure est utilise en general pour evaluer des similarites sur 
des jeux de donnees ou les amplitudes des distances inter individus importe 
peu.
'''
def cosine(x, y):
    return np.dot(x, y) / ( np.sqrt(np.dot(x, x)) * np.sqrt(np.dot(y, y)) )

'''
Retourne la matrice des distances angulaires a partir d'un jeu de positions 
entrees en parametres
'''
def cosine_matrix(points):
    # On determine le nombre de points a traiter
    n_nodes = len(points)
    # On initialise la matrice angulaire a retourner pour une taille de n_nodes x n_nodes
    cosine_matrix = [ [0.0] * n_nodes for _ in range(n_nodes) ]
    # Pour chaque points definis dans la liste des positions on recupere l'angle les separant et on 
    # met a jour la matrice des distances angulaires
    for i in range(n_nodes):
        for j in range(n_nodes):
            cosine_distance = distance.cosine(
                points[i], 
                points[j]
            )
            if cosine_distance < EPSILON:
                cosine_matrix[i][j] = 0.0
            else: 
                cosine_matrix[i][j] = cosine_distance
    return cosine_matrix

'''
Cette metrique mesure la distance euclidienne separant deux points x et y 
fournis en parametres.
Elle est utile notamment lorsque l'on souhaite mettre en exergue des effets 
d'amplitude dans le jeu de donnees en presence.
'''
def euclidean(x, y):
    return np.sqrt(np.sum( (x - y) ** 2 ))

'''
Retourne la matrice des distances euclidiennes a partir d'un jeu de positions 
entrees en parametres
'''
def euclidean_matrix(points):
    # On determine le nombre de points a traiter
    n_nodes = len(points)
    # On initialise la matrice angulaire a retourner pour une taille de (n_nodes x n_nodes)
    euclidean_matrix = [ [0.0] * n_nodes for _ in range(n_nodes) ]
    # Pour chaque points definis dans la liste des positions on recupere la distance les separant et
    # on met a jour la matrice des distances euclidiennes
    for i in range(n_nodes):
        for j in range(n_nodes):
            if i != j:
                euclidean_matrix[i][j] = euclidean(
                    points[i], 
                    points[j]
                )
    return euclidean_matrix
