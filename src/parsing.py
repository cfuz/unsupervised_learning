#!/usr/bin/python3.7
# coding: utf-8
import networkx as nx
import numpy as np
import os
import sys

from message_handling import show_message


# Meta-parameters
EXPECTED_N_ARGS = 3
OPTIONAL_N_ARGS = 1
OPTIONAL_INDEX = EXPECTED_N_ARGS + OPTIONAL_N_ARGS


'''
Traite les arguments renseignés par l'utilisateur
'''
def parse_arguments():
    check_number_of_arguments()
    parsed_arguments = {}
    check_optional(parsed_arguments)
    parsed_arguments.update(setup_output_file_system(parsed_arguments['explicit']))
    return parsed_arguments

'''
Vérifie le nombre d'arguments.
Sort du système si jamais les paramètres en entrée de script ne sont pas bien 
renseignés par l'utilisateur
'''
def check_number_of_arguments():
	if len(sys.argv) < EXPECTED_N_ARGS + 1:
		print("✘  Wrong number of arguments given. Expected at least {}, gave {}".format(
			EXPECTED_N_ARGS,
			len(sys.argv) - 1
		))
		print("✘  Input command from project's root directory should be of type:")
		print("   python src/community_detection.py input_file output_directory/ ratio [-e | --explicit]")
		sys.exit()
	elif len(sys.argv) > EXPECTED_N_ARGS + OPTIONAL_N_ARGS + 1:
		print("✘  Wrong number of arguments given. Expected at most {}, gave {}".format(
			OPTIONAL_INDEX,
			len(sys.argv) - 1
		))
		print("✘  Input command from project's root directory should be of type:")
		print("   python src/community_detection.py input_file output_directory [-e | --explicit]")
		sys.exit()

'''
Traite le nom et chemin du fichier d'entrée pour paramétrer le nom du fichier 
en sortie avec les dossiers qui conviennent.
'''
def setup_output_file_system(explicit_mode):
	# Extraction du nom du fichier d'entree sans son extension
	input_basename = os.path.basename(sys.argv[1])
	input_directory = os.path.dirname(sys.argv[1])
	input_basename = os.path.splitext(input_basename)[0]
	input_community = "{}/{}".format(
		input_directory, 
		"community.dat"
	)
	# Nom du fichier de sortie
	output_filename = sys.argv[2] + input_basename 
	# Nom du fichier contenant les donnees de modularite
	data_directory = sys.argv[2] + "data/"
	try:
		os.makedirs(data_directory) 
	except FileExistsError:
		show_message(
            "✔  Directory {} already exists. It will be used for output .dat files storing..".format(data_directory),
            explicit_mode
        )
	data_filename = data_directory + input_basename + "_output.dat"
	return {
		'input_basename': input_basename, 
		'input_dir': input_directory,
        'input': sys.argv[1],
		'communities': input_community, 
		'output_html': output_filename,
        'output_data': data_filename,
		'data_dir': data_directory,
        'benchmark_data': data_directory + "benchmark.dat",
		'expected': False
	}

'''
Vérifie la présence du paramètre optionnel de l'application
'''
def check_optional(map_to_update):
    map_to_update['explicit'] = False
    if len(sys.argv) == EXPECTED_N_ARGS + OPTIONAL_N_ARGS + 1:
        if (sys.argv[OPTIONAL_INDEX] == "-e" or sys.argv[OPTIONAL_INDEX] == "--explicit"):
            map_to_update['explicit'] = True
        else: 
            print("✘  Optional argument {} unknown. If you want a detailed output please use -e or --explicit.".format(
                sys.argv[OPTIONAL_INDEX] 
            ))
            silent_mode = input("➜  Continue in silent mode ? [Y/n] ").lower()
            if silent_mode == "n" or silent_mode == "no":
                print("➜  Starting clusterization in explicit mode..")
                map_to_update['explicit'] = True
            else:
                print("➜  Starting clusterization in silent mode..")


'''
Construit un graphe a partir d'une liste d'arcs renseignes dans un fichier 
filename
    --> Lien vers la librairie networkx pour la construction de graphes : 
    https://networkx.github.io/documentation/stable/tutorial.html
'''
def parse_graph(edges_filename):
    graph = nx.Graph()
    edges = []
    # Ouverture et lecture du fichier contenant les informations du graphe
    # n.b. 
    #   On supposera que les entrees du fichiers prennent la forme "source\tdestination\n"
    #   On supposera egalement que le graphe demande est un graphe non-oriente
    file = open(edges_filename, "r")
    n_instances = int(file.readline())
    graph.add_nodes_from(np.arange(1, n_instances))
    for entry in file:
    	buffer = entry.split("\n")[0].split("\t")
    	edges.append((
    		int(buffer[0]), 
    		int(buffer[1])
    	))  
    graph.add_edges_from(edges)
    file.close()
    return graph

'''
Attribue les groupes d'appartenance aux noeuds d'un graphe a partir des
donnees issues d'un fichier
'''
def parse_clusters(graph, clusters_filename):
    try:
        file = open(clusters_filename, "r")
        for input in file:
            buffer = input.split("\n")[0].split("\t")
            graph.nodes[int(buffer[0])]['cluster'] = int(buffer[1])
    except IOError:
        return False
    finally:
        file.close()
        return True

'''
Parse le fichier contenant les donnees sur la modularite pour le fichier 
renseigne en parametre
'''
def parse_modularities(filename):
    file = open(filename, "r")
    modularities = {}
    for input in file:
        buffer = input.split("\n")[0].split("\t")
        if buffer[0] in modularities:
            modularities[buffer[0]].append((
                int(buffer[1]), # hauteur de la fusion
                float(buffer[2]) # valeur de la modularite en ce point
            ))
        else:
            modularities[buffer[0]] = [(
                int(buffer[1]),
                float(buffer[2])
            )]
    return modularities

